﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;

public class PictureImage : MonoBehaviour,IInputClickHandler {
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnInputClicked(InputClickedEventData eventData)
    {
        //throw new NotImplementedException();
        gameObject.transform.parent.gameObject.GetComponent<SpatialNote.NoteBehavior>().Detail_onClick();

        eventData.Use();
    }
}
