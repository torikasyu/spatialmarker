﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SpatialNote;

public class FadeEffect : MonoBehaviour {

    Color cl;
    Hashtable hash = null;

    // Use this for initialization
    void Awake () {

        Debug.Log("START!!");
        hash = new Hashtable(){
                {"from", 0f},
                {"to", 1f},
                {"time", 1f},
                {"delay", 0f},
                {"easeType",iTween.EaseType.linear}, //easeInQuart
                {"loopType",iTween.LoopType.none},
                {"onupdate", "SetValue"},
                {"onupdatetarget", gameObject},
            };
    }
	
    public void StartFadeALL(FadeType type)
    {
        hash["onupdate"] = "SetValueRecursive";
        if (type == FadeType.FadeIn)
        {
            hash["from"] = 0f;
            hash["to"] = 1f;
        }
        else if (type == FadeType.FadeOut)
        {
            hash["from"] = 1f;
            hash["to"] = 0f;
            hash["oncomplete"] = "destroyAfterTween";
        }
        else if (type == FadeType.FadeOutHarf)
        {
            hash["from"] = 1f;
            hash["to"] = 0.2f;
        }
        iTween.ValueTo(gameObject, hash);
    }

    public void StartBlinkColor(GameObject target,Color origin, Color highlight)
    {
        hash["onupdate"] = "SetColorValue";
        hash["from"] = 0f;
        hash["to"] = 1f;
        hash["loopType"] = iTween.LoopType.pingPong;
        hash["time"] = 0.5f;

        originColor = origin;
        highlightColor = highlight;

        StartCoroutine(StopTween(target,3f));

        iTween.ValueTo(target, hash);


    }

    IEnumerator StopTween(GameObject target,float time)
    {
        yield return new WaitForSeconds(time);
        iTween.Stop(target);
        SetColorValue(1f);
    }

    Color originColor;
    Color highlightColor;

    void SetColorValue(float rate)
    {
        Color col = Color.Lerp(originColor, highlightColor, rate);
        gameObject.GetComponent<NoteBehavior>().SetNoteColor(col);
    }

    void destroyAfterTween()
    {
        print("destroy");
        Destroy(gameObject);
    }

    //iTweenから呼ばれる
    public void SetValueRecursive(float alpha)
    {
        SetAlphaRecursive(gameObject,alpha);
    }

    //再帰的にアルファを変更する
    void SetAlphaRecursive(GameObject obj,float alpha)
    {
        SetAlpha(obj,alpha);
        foreach (Transform tfm in obj.transform)
        {
            SetAlpha(tfm.gameObject, alpha);
            SetAlphaRecursive(tfm.gameObject,alpha);
        }
    }

    //TextまたはRenderer.Materialのアルファを変更する
    void SetAlpha(GameObject obj,float alpha)
    {
        if (obj.GetComponent<Text>() != null)
        {
            Material m = obj.GetComponent<Text>().material;
            Color newColor = new Color(m.color.r, m.color.g, m.color.b, alpha);
            m.color = newColor;
        }
        else if (obj.GetComponent<Renderer>() != null)
        {
            Material m = obj.GetComponent<Renderer>().material;
            Color newColor = new Color(m.color.r, m.color.g, m.color.b, alpha);
            m.color = newColor;
        }
    }

    public enum FadeType
    {
        FadeIn,
        FadeOut,
        FadeOutHarf,
        ScaleLarger,
        ScaleSmaller
    }

    Vector3 originScale;

    public void StartScale(FadeType type)
    {
        originScale = gameObject.transform.localScale;

        hash["onupdate"] = "SetScale";
        hash["time"] = 3f;

        if (type == FadeType.ScaleLarger)
        {
            hash["from"] = 0f;
            hash["to"] = 1f;
        }
        else if(type == FadeType.ScaleSmaller)
        {
            hash["from"] = 1f;
            hash["to"] = 0.05f;
            //hash["easeType"] = iTween.EaseType.easeInQuart;
            hash["oncomplete"] = "destroyAfterTween";
        }
        iTween.ValueTo(gameObject, hash);
    }

    void SetScale(float size)
    {
        gameObject.transform.localScale = Vector3.Scale(originScale, new Vector3(size, size, 1));
    }
}
