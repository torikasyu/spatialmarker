﻿using UniRx;
using System;


public interface IInfomationMessageSender
{
    IObservable<string> OnMessageChanged { get; }
}


