﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.SpatialMapping;
using UnityEngine.XR.WSA.Persistence;
using UnityEngine.XR.WSA.WebCam;
using System.Linq;
using UniRx;
using System;
using System.IO;

namespace SpatialNote
{
    public class SpatialNoteManager : Singleton<SpatialNoteManager>, IInputClickHandler, IDictationHandler,IInfomationMessageSender
    {

        [SerializeField]
        GameObject MarkerPrefab;
        [SerializeField]
        GameObject Reticle;
        [SerializeField]
        GameObject CircleFrame;
        [SerializeField]
        GameObject MainMenu;

        [Tooltip("Distance from camera to keep the object while placing it.")]
        [SerializeField]
        float DefaultGazeDistance = 2.0f;
        [SerializeField]
        Color normalColor = Color.grey;
        [SerializeField]
        Color nextColor = Color.blue;

        [SerializeField]
        GameObject helpPanel;

        [HideInInspector]
        public GameObject targetMarker;
        [HideInInspector]
        public GameObject detailWindow;

        public enum AppMode
        {
            none = -1,
            Edit,
            Play
        }

        public AppMode appMode = AppMode.none;

        public Color NormalColor { get { return normalColor; } }
        public Color NextColor { get { return nextColor; } }


        #region private_variables
        int CurrentPageNo = 0;

        PhotoCapture photoCaptureObject = null;
        public enum EnumBehaveMode
        {
            //none = -1,
            CreateNote,
            PlacingNote,
            PhotoCapture,
            Recording
        }

        [SerializeField]
        EnumBehaveMode behaveMode = EnumBehaveMode.CreateNote;
        public EnumBehaveMode BehaveMode
        {
            get { return this.behaveMode; }
        }

        string photoFileName = string.Empty;

        List<GameObject> noteList = new List<GameObject>();

        #endregion

        #region Message
        public Subject<string> subject = new Subject<string>();
        public IObservable<string> OnMessageChanged{get{return subject;}}
        #endregion

        /// <summary>
        /// 次のノートにナビゲーションを行う
        /// </summary>
        /// <param name="currentNo"></param>
        public void NextNoteNavigation(int currentNo)
        {
            print("currentNo" + currentNo);
            print("count" + noteList.Count);

            if (noteList.Count <= currentNo + 1)
            {
                print("next note is not exists.");
                return;
            }

            //いったん全てのノートを通常色にする
            foreach (GameObject go in noteList)
            {
                go.GetComponent<NoteBehavior>().SetNoteColor(normalColor);
            }

            GameObject note = noteList[currentNo + 1];

            //次のノートから音を出す
            note.GetComponent<NoteBehavior>().SetNoteAsNextTarget();

            //次のノートを点滅させる
            //note.GetComponent<FadeEffect>().StartBlinkColor(note.GetComponent<NoteBehavior>().frame,normalColor, nextColor);

            //収束する円を描画する
            GameObject circle = Instantiate(CircleFrame,note.transform.position,note.transform.rotation);
            circle.GetComponent<FadeEffect>().StartScale(FadeEffect.FadeType.ScaleSmaller);
        }

        public void MenuNavigation()
        {
            //次のノートから音を出す
            MainMenu.GetComponent<MainMenu>().DoSound();
            //収束する円を描画する
            GameObject circle = Instantiate(CircleFrame, MainMenu.transform.position, MainMenu.transform.rotation);
            circle.GetComponent<FadeEffect>().StartScale(FadeEffect.FadeType.ScaleSmaller);
        }

        /// <summary>
        /// グローバルタップ
        /// behaveModeに応じた反応をする
        /// </summary>
        /// <param name="eventData"></param>
        public void OnInputClicked(InputClickedEventData eventData)
        {
            switch (behaveMode)
            {
                case EnumBehaveMode.CreateNote:

                    if (appMode != AppMode.Edit) return;

                    if (IsExceptSpatialMappingRaycast(CameraCache.Main.transform.position,CameraCache.Main.transform.forward)) return;

                    Vector3 placementPosition = GetPlacementPosition(CameraCache.Main.transform.position, CameraCache.Main.transform.forward, DefaultGazeDistance);
                    if (placementPosition == Vector3.zero)
                    {
                        subject.OnNext("Can't Create at this place");
                    }
                    else
                    {
                        if (noteList.Count > 98)
                        {
                            subject.OnNext("Notes limit reached");
                        }
                        else
                        {
                            //ノート生成
                            var MarkerID = "N" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                            var markerObj = Instantiate(MarkerPrefab, placementPosition, Quaternion.identity);
                            markerObj.SetActive(true);
                            markerObj.name = MarkerID;
                            markerObj.GetComponent<MyTapToPlace>().IsBeingPlaced = true;
                            markerObj.GetComponent<NoteBehavior>().SetNoteColor(color: normalColor);

                            noteList.Add(markerObj);
                            SetSeqNo();
                        }
                    }

                    eventData.Use();
                    break;

                case EnumBehaveMode.PhotoCapture:

                    PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);

                    behaveMode = EnumBehaveMode.CreateNote;
                    eventData.Use();

                    break;

                case EnumBehaveMode.Recording:

                    //this.targetMarker.GetComponent<NoteBehavior>().StartRecord_onClick();
                    StartCoroutine(DictationInputManager.StopRecording());
                    //behaveMode = EnumBehaveMode.CreateNote;
                    eventData.Use();

                    break;

                /*
                case EnumBehaveMode.none:
                    //through event
                    break;
                    */

                default:
                    break;

            }

        }

        /// <summary>
        /// ノートを管理リストから削除してDestroyする
        /// </summary>
        /// <param name="note"></param>
        public void DeleteNote(GameObject note)
        {
            //WorldAnchorManagerを使わないで直接削除（対象GameObjectがDestroyされているとエラーになるため）
#if UNITY_WSA && !UNITY_EDITOR
            if(WorldAnchorManager.Instance.AnchorStore != null)
            {
                WorldAnchorManager.Instance.AnchorStore.Delete(note.name);
            }
            else
            {
                Debug.LogWarning("AnchorStore is not Ready");
            }
#endif
            //管理リストから削除
            noteList.Remove(note);
            //シーケンスがずれるので再表示
            SetSeqNo();
            Destroy(note);
        }

        public void DeleteAllNote()
        {
            if (SpatialNoteManager.Instance.appMode != AppMode.Edit) return;

            for (int i = 0; i < noteList.Count; i++)
            {
#if UNITY_WSA && !UNITY_EDITOR
            if(WorldAnchorManager.Instance.AnchorStore != null)
            {
                WorldAnchorManager.Instance.AnchorStore.Delete(noteList[i].name);
            }
            else
            {
                Debug.LogWarning("AnchorStore is not Ready");
            }
#endif
                Destroy(noteList[i]);
            }
            noteList.Clear();
        }

        //シーケンスナンバーを再表示する
        void SetSeqNo()
        {
            bool nextButtonVisible = true;
            for (int i=0; i < noteList.Count; i++)
            {
                //最後のノートはNextボタンを出さない
                if (i == noteList.Count - 1) nextButtonVisible = false;
                noteList[i].GetComponent<NoteBehavior>().SetSeqNo(no:i,nextButtonVisible:nextButtonVisible);
            }
        }

        /// <summary>
        /// EDITとPLAYモードを切り替える
        /// </summary>
        /// <param name="mode"></param>
        public void SetAppMode(AppMode mode)
        {
            appMode = mode;
            Destroy(detailWindow);

            if (appMode == AppMode.Edit)
            {
                SpatialMappingManager.Instance.DrawVisualMeshes = true;
            }
            else
            {
                SpatialMappingManager.Instance.DrawVisualMeshes = false;

                //最初のノートに誘導する
                if (noteList.Count > 0)
                {
                    NextNoteNavigation(-1);
                }
            }

            //各ノートの編集ボタンを制御する
            foreach (GameObject go in noteList)
            {
                NoteBehavior mb = go.GetComponent<NoteBehavior>();
                mb.SetEditButtonActive(appMode == AppMode.Edit);
                //色をすべて戻す
                mb.SetNoteColor(normalColor);
            }
        }

        public void SetCreateNoteMode()
        {
            this.behaveMode = EnumBehaveMode.CreateNote;
        }

        public void SetPlaceNoteMode()
        {
            subject.OnNext("Tap to Place Note");
            this.behaveMode = EnumBehaveMode.PlacingNote;
        }

        public void SetPhotoCaptureMode(string fileName,GameObject marker)
        {
            this.targetMarker = marker;

            subject.OnNext("Tap to Take Picture");
            this.behaveMode = EnumBehaveMode.PhotoCapture;
            this.photoFileName = fileName;

            this.Reticle.SetActive(true);
        }

        public void SetRecordingMode(GameObject marker)
        {
            this.targetMarker = marker;
            subject.OnNext("Tap to finish Recognize");
            this.behaveMode = EnumBehaveMode.Recording;

            StartCoroutine(DictationInputManager.StartRecording());
        }

        public void ShowHelp()
        {
            helpPanel.SetActive(true);
        }

        // Use this for initialization
        void Start() {

            appMode = AppMode.Edit;
            subject.OnNext("Tap to Place Note");

            InputManager.Instance.AddGlobalListener(gameObject);

            //写真用照準を非表示
            this.Reticle.SetActive(false);

            //起動時はメモ作成モード
            behaveMode = EnumBehaveMode.CreateNote;

#if UNITY_WSA && !UNITY_EDITOR
            //WorldAnchorが存在すれば復元
            WorldAnchorStore.GetAsync(AnchorStoreReady);
#endif

            StartCoroutine(ShowCreateMessage());

            //初回チュートリアル
            int sts = PlayerPrefs.GetInt("AutoHelp",0);
            if (sts == 0) { this.ShowHelp(); }

            /*
            MarkerData d = new MarkerData();
            d.MarkerID = "hogehoge";
            d.Description = "fugagfuga";
            PlayerPrefsUtils.SetObject<MarkerData>("aaa", d);
            var r = PlayerPrefsUtils.GetObject<MarkerData>("aaa");
            print("DescTest "+r.Description);
            */
        }

        // Update is called once per frame
        void Update(){}

        IEnumerator ShowCreateMessage()
        {
            while (true)
            {
                if (appMode == AppMode.Edit && behaveMode == EnumBehaveMode.CreateNote)
                {
                    if (IsSpatialMappingRaycast() && !IsExceptSpatialMappingRaycast())
                    {
                        subject.OnNext("Tap to Create Note");
                    }
                    else if (IsNoteRayCast())
                    {
                        subject.OnNext("Tap to Move Note");
                    }
                    else if (IsNoteImageRayCast())
                    {
                        subject.OnNext("Tap to Zoom");
                    }
                    else
                    {
                        subject.OnNext("");
                    }
                }
                else if(appMode == AppMode.Play)
                {
                    if (IsNoteImageRayCast())
                    {
                        subject.OnNext("Tap to Zoom");
                    }
                    else
                    {
                        subject.OnNext("");
                    }
                }
                yield return new WaitForSeconds(0.5f);
            }
        }

        /// <summary>
        /// アプリ起動時に呼ばれるWorldAnchorの復元処理
        /// </summary>
        /// <param name="store"></param>
        void AnchorStoreReady(WorldAnchorStore store)
        {
            if (WorldAnchorManager.Instance == null)
            {
                Debug.Log("WorldAnchorManager is null");
                return;
            }
            if (store == null)
            {
                Debug.Log("GetAsynced AnchorStore is null");
                return;
            }

            List<GameObject> tempList = new List<GameObject>();

            //アンカーの復元
            string[] ids = store.GetAllIds();
            foreach (string objName in ids)
            {
                Debug.Log("anchorid:" + objName);

                //アンカーの分Instantiateする
                //内容はInstantinate時に各オブジェクトのStartで行う
                var markerObj = Instantiate(MarkerPrefab);
                markerObj.name = objName;
                markerObj.SetActive(true);
                markerObj.GetComponent<NoteBehavior>().SetNoteColor(color: normalColor);

                tempList.Add(markerObj);

                //WorldAnchorをLoadする
                markerObj.GetComponent<MyTapToPlace>().IsBeingPlaced = true;
                store.Load(objName, markerObj); //Attachしてはいけない
                markerObj.GetComponent<MyTapToPlace>().IsBeingPlaced = false;
            }

            var query = from obj in tempList
                        orderby obj.name
                        select obj;

            foreach (GameObject note in query.ToArray())
            {
                print(note.name);
            }

            noteList.AddRange(query.ToArray());
            SetSeqNo();
        }

#region RayCastMetods
        /// <summary>
        /// If we're using the spatial mapping, check to see if we got a hit, else use the gaze position.
        /// </summary>
        /// <returns>Placement position in front of the user</returns>
        Vector3 GetPlacementPosition(Vector3 headPosition, Vector3 gazeDirection, float defaultGazeDistance)
        {
            RaycastHit hitInfo;

            if (IsExceptSpatialMappingRaycast(headPosition, gazeDirection))
            {
                //print("hit except spatial mapping");
                return Vector3.zero;
            }

            if (SpatialMappingRaycast(headPosition, gazeDirection, out hitInfo))
            {
                return hitInfo.point;
            }
            //return GetGazePlacementPosition(headPosition, gazeDirection, defaultGazeDistance);
            return Vector3.zero;
        }

        /// <summary>
        /// Does a raycast on the spatial mapping layer to try to find a hit.
        /// </summary>
        /// <param name="origin">Origin of the raycast</param>
        /// <param name="direction">Direction of the raycast</param>
        /// <param name="spatialMapHit">Result of the raycast when a hit occurred</param>
        /// <returns>Whether it found a hit or not</returns>
        bool SpatialMappingRaycast(Vector3 origin, Vector3 direction, out RaycastHit spatialMapHit)
        {
            if (SpatialMappingManager.Instance != null)
            {
                RaycastHit hitInfo;
                if (Physics.Raycast(origin, direction, out hitInfo, 30.0f, SpatialMappingManager.Instance.LayerMask))
                {
                    spatialMapHit = hitInfo;
                    return true;
                }
            }
            spatialMapHit = new RaycastHit();
            return false;
        }

        /// <summary>
        /// SpatialMappingにRaycastHitしているかどうか
        /// </summary>
        /// <returns></returns>
        bool IsSpatialMappingRaycast()
        {
            if (SpatialMappingManager.Instance != null)
            {
                RaycastHit hitInfo;
                if (Physics.Raycast(CameraCache.Main.transform.position, CameraCache.Main.transform.forward, out hitInfo, 30.0f, SpatialMappingManager.Instance.LayerMask))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// SpatialMapping以外のオブジェクトにRaycastHitしているかどうか
        /// </summary>
        /// <param name="origin">Origin of the raycast</param>
        /// <param name="direction">Direction of the raycast</param>
        /// <returns></returns>
        bool IsExceptSpatialMappingRaycast(Vector3 origin, Vector3 direction)
        {
            int layerMask = ~(1 << SpatialMappingManager.Instance.PhysicsLayer);
            RaycastHit hitInfo;
            if (Physics.Raycast(origin, direction, out hitInfo, 30.0f, layerMask))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// SpatialMapping以外のオブジェクトにRaycastHitしているかどうか
        /// </summary>
        /// <returns></returns>
        bool IsExceptSpatialMappingRaycast()
        {
            return IsExceptSpatialMappingRaycast(CameraCache.Main.transform.position,CameraCache.Main.transform.forward);
        }

        bool IsNoteImageRayCast()
        {
            return IsTagRayCast("note_image");            
        }

        bool IsNoteRayCast()
        {
            return IsTagRayCast("note");
        }

        bool IsTagRayCast(string tag)
        {
            int layerMask = ~(1 << SpatialMappingManager.Instance.PhysicsLayer);
            RaycastHit hitInfo;
            if (Physics.Raycast(CameraCache.Main.transform.position, CameraCache.Main.transform.forward, out hitInfo, 30.0f, layerMask))
            {
                if (hitInfo.collider.gameObject.tag == tag)
                {
                    return true;
                }
            }
            return false;
        }

#endregion

#region PhotoCaptureMethods

        void OnPhotoCaptureCreated(PhotoCapture captureObject)
        {
            photoCaptureObject = captureObject;

            Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();

            CameraParameters c = new CameraParameters();
            c.hologramOpacity = 0.0f;
            c.cameraResolutionWidth = cameraResolution.width;
            c.cameraResolutionHeight = cameraResolution.height;
            c.pixelFormat = CapturePixelFormat.BGRA32;

            captureObject.StartPhotoModeAsync(c, OnPhotoModeStarted);
        }

        void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
        {
            photoCaptureObject.Dispose();
            photoCaptureObject = null;

            this.Reticle.SetActive(false);
        }

        private void OnPhotoModeStarted(PhotoCapture.PhotoCaptureResult result)
        {
            if (result.success)
            {
                //string filename = string.Format(@"CapturedImage{0}_n.jpg", Time.time);
                string filePath = System.IO.Path.Combine(Application.persistentDataPath, this.photoFileName);

                print(filePath);

                photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
                photoCaptureObject.TakePhotoAsync(filePath, PhotoCaptureFileOutputFormat.JPG, OnCapturedPhotoToDisk);
            }
            else
            {
                Debug.LogError("Unable to start photo mode!");
            }
        }

        private void OnCapturedPhotoToMemory(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
        {
            if (result.success)
            {
                List<byte> imageBufferList = new List<byte>();
                // Copy the raw IMFMediaBuffer data into our empty byte list.
                photoCaptureFrame.CopyRawImageDataIntoBuffer(imageBufferList);

                //画像表示処理呼び出し
                DisplayImage(imageBufferList.ToArray());                            
            }
        }

        private void DisplayImage(byte[] imageData)
        {
            Texture2D imageTxtr = new Texture2D(2, 2);
            imageTxtr.LoadImage(imageData);
        }

        void OnCapturedPhotoToDisk(PhotoCapture.PhotoCaptureResult result)
        {
            print(result.resultType.ToString());

            if (result.success)
            {
                Debug.Log("Saved Photo to disk!");
                photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
                subject.OnNext("Picture Saved:" + photoFileName);

                //ReadPicture(photoFileName);
                Texture2D texture = Utilities.ReadPictureFromDataPath(photoFileName);
                this.targetMarker.GetComponent<NoteBehavior>().ImagePhoto.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
            }
            else
            {
                Debug.Log("Failed to save Photo to disk");
                photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
                subject.OnNext("Failed to take picture.");
            }
        }
        #endregion

        #region Dictation
        string dictationResult = "";

        public void OnDictationHypothesis(DictationEventData eventData)
        {
            dictationResult = eventData.DictationResult;
            targetMarker.GetComponent<NoteBehavior>().UiText_desc.text = dictationResult;
        }

        public void OnDictationResult(DictationEventData eventData)
        {
            dictationResult = eventData.DictationResult;
            targetMarker.GetComponent<NoteBehavior>().UiText_desc.text = dictationResult;
        }

        public void OnDictationComplete(DictationEventData eventData)
        {
            print("Dictation Complete");

            var dictationResult = eventData.DictationResult;

            targetMarker.GetComponent<NoteBehavior>().UiText_desc.text = dictationResult;

            //データ保存
            targetMarker.GetComponent<NoteBehavior>().SaveDescription(dictationResult);

            this.behaveMode = EnumBehaveMode.CreateNote;
            this.targetMarker.GetComponent<NoteBehavior>().EndRecord();
        }

        public void OnDictationError(DictationEventData eventData)
        {
            print("Dictation Error");

            StartCoroutine(DictationInputManager.StopRecording());
            this.behaveMode = EnumBehaveMode.CreateNote;
            this.targetMarker.GetComponent<NoteBehavior>().EndRecord();

        }

        #endregion


    }
}