﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace SpatialNote
{
    public static class Utilities
    {

        /// <summary>
        /// persistentDataPathから指定されたファイルの画像のTextureを得る
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static Texture2D ReadPictureFromDataPath(string fileName)
        {
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, fileName);

            byte[] bytes;
            Texture2D texture = null;

            try
            {
                using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    BinaryReader bin = new BinaryReader(fileStream);
                    bytes = bin.ReadBytes((int)bin.BaseStream.Length);
#if UNITY_WSA && !UNITY_EDITOR
                bin.Dispose();
#else
                    bin.Close();
#endif
                }

                texture = new Texture2D(1, 1);
                texture.LoadImage(bytes);
            }
            catch(Exception ex)
            {
                Debug.Log(ex.Message);
                return null;
            }

            return texture;
        }

        public static void DeletePictureFromDataPath(string fileName)
        {
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, fileName);

            File.Delete(filePath);
        }
    }

    public static class PlayerPrefsUtils
    {
        /// <summary>
        /// 指定されたオブジェクトの情報を保存します
        /// </summary>
        public static void SetObject<T>(string key, T obj)
        {
            var json = JsonUtility.ToJson(obj);
            PlayerPrefs.SetString(key, json);

            PlayerPrefs.Save();
        }

        /// <summary>
        /// 指定されたオブジェクトの情報を読み込みます
        /// </summary>
        public static T GetObject<T>(string key)
        {
            var json = PlayerPrefs.GetString(key);
            var obj = JsonUtility.FromJson<T>(json);
            return obj;
        }
    }

    [Serializable]
    public class MarkerData
    {
        public string MarkerID = "";
        public string Description = "";
        //public string ImageName = "";
        public string ImageName
        {
            get { return MarkerID + ".jpg"; }
        }
        //public int SeqNo = 0;

    }

}