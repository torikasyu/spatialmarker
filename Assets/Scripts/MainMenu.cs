﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Examples.InteractiveElements;
using System;

namespace SpatialNote
{
    public class MainMenu : MonoBehaviour
    {

        public GameObject button_play;
        public GameObject button_edit;
        public GameObject button_reset;

        InteractiveToggle it_play;
        InteractiveToggle it_edit;

        // Use this for initialization
        void Start()
        {
            it_play = button_play.GetComponent<InteractiveToggle>();
            it_edit = button_edit.GetComponent<InteractiveToggle>();

            button_edit_onClick();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void button_play_onClick()
        {
            it_play.SetSelection(true);
            it_edit.SetSelection(false);

            button_reset.SetActive(false);

            SpatialNoteManager.Instance.SetAppMode(mode: SpatialNoteManager.AppMode.Play);
        }

        public void button_edit_onClick()
        {
            it_play.SetSelection(false);
            it_edit.SetSelection(true);

            button_reset.SetActive(true);

            SpatialNoteManager.Instance.SetAppMode(mode: SpatialNoteManager.AppMode.Edit);
        }

        public void button_help_onClick()
        {
            SpatialNoteManager.Instance.ShowHelp();
        }

        public void button_reset_onClick()
        {
            SpatialNoteManager.Instance.DeleteAllNote();
        }

        internal void DoSound()
        {
            //throw new NotImplementedException();
            AudioClip pong = Resources.Load<AudioClip>("decision3");
            GetComponent<AudioSource>().PlayOneShot(pong);
        }
    }
}
