﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpPanel : MonoBehaviour {

    [SerializeField]
    GameObject button_next;
    [SerializeField]
    TextMesh button_next_text;
    [SerializeField]
    TextMesh button_language_text;
    [SerializeField]
    Image helpImage;
    [SerializeField]
    Sprite[] images_e;
    [SerializeField]
    Sprite[] images_j;


    int index = 0;
    int language = 1;

    public void button_next_onClick()
    {
        index++;

        if (index == 4)
        {
            SpatialNote.SpatialNoteManager.Instance.MenuNavigation();


            PlayerPrefs.SetInt("AutoHelp", 1);
            PlayerPrefs.Save();

            index = 0;

            gameObject.SetActive(false);

        }

        ShowImage();
    }

    public void button_language_onClick()
    {
        if (language == 0) {
            language = 1;
            button_language_text.text = "English";
        } else {
            language = 0;
            button_language_text.text = "Japanese";
        }

        index = 0;
        ShowImage();
    }

    void ShowImage()
    {
        if (language == 0)
        {
            this.helpImage.sprite = images_e[index];
        }
        else
        {
            this.helpImage.sprite = images_j[index];
        }

        if (index == 3)
        {
            button_next_text.text = "Close";
        }
        else
        {
            button_next_text.text = "Next";
        }
    }

    // Use this for initialization
    void Start () {
        ShowImage();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
