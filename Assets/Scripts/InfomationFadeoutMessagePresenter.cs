﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;


[RequireComponent(typeof(TextMesh))]
public class InfomationFadeoutMessagePresenter : MonoBehaviour
{
    [Tooltip("メッセージ送信元のオブジェクト 要IInfomationMessageSenderを実装しているコンポーネント")] public GameObject[] SenderObjects;
    [Tooltip("フェードアウトするまでの待ち時間(秒)")] public float WaitFadeoutSeconds = 3f;

    // Use this for initialization
    void Start()
    {
        var textMesh = GetComponent<TextMesh>();

        // 初期表示は空にする
        textMesh.text = string.Empty;

        // メッセージ送信Observableをすべて取得
        var senderObservables = SenderObjects
            .SelectMany(x => x.GetComponents<IInfomationMessageSender>())
            .Select(x => x.OnMessageChanged);

        // ストリームをマージしてメッセージの更新を行う
        // Completeした場合は非表示にする
        var mergeObservables = senderObservables.Merge();
        mergeObservables.Subscribe(x =>
        {
            textMesh.text = x;
            iTween.Stop(gameObject);
            iTween.FadeTo(gameObject, iTween.Hash(
                "alpha", 1f,
                "time", 0.1f,
                "delay", 0.01f));
        }, () => gameObject.SetActive(false));

        // 一定時間メッセージがなければフェードアウトする
        mergeObservables.Throttle(TimeSpan.FromSeconds(WaitFadeoutSeconds)).Subscribe(_ => iTween.FadeTo(gameObject, 0f, 0.1f));
    }
}

