﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteDetailBehaviour : MonoBehaviour {

    public Image ImagePhoto;
    public Text Text_Description;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onClick_Button_Close()
    {
        //Destroy(gameObject);
        Destroy(SpatialNote.SpatialNoteManager.Instance.detailWindow);
    }
}
