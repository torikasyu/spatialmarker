﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using UnityEngine.UI;
using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System;
using SpatialNote;

namespace SpatialNote
{

    public class NoteBehavior : MonoBehaviour
    {
        [SerializeField]
        GameObject button_Remove;
        [SerializeField]
        GameObject button_Text;
        [SerializeField]
        GameObject button_Picture;
        [SerializeField]
        GameObject button_Next;

        [SerializeField]
        GameObject frame;

        [SerializeField]
        Image imagePhoto;
        [SerializeField]
        Text uiText_desc;
        [SerializeField]
        Text uiText_seq;

        [SerializeField]
        TextMesh button_Text_label;

        [SerializeField]
        GameObject DetailCanvasPrefab;

        int SeqNo = 0;

        MarkerData markerData = null;
        public MarkerData MarkerData
        {
            get { return markerData; }
        }

        public Image ImagePhoto { get { return imagePhoto; } set { value = imagePhoto; } }
        public Text UiText_desc { get { return uiText_desc; } set { value = uiText_desc; } }

        //bool isRecording = false;

        /*
        bool isShowCamera = false;
        void OnWillRenderObject()
        {
#if UNITY_EDITOR
            if (Camera.current.name != "SceneCamera" && Camera.current.name != "Preview Camera")
#endif
            {
                isShowCamera = true;
            }
        }
        */

        // Use this for initialization
        void Start()
        {
            var data = PlayerPrefsUtils.GetObject<MarkerData>(this.gameObject.name);

            if (data == null)
            {
                print("new data");
                //新規データ保存
                data = new MarkerData();
                data.MarkerID = this.gameObject.name;
                PlayerPrefsUtils.SetObject<MarkerData>(this.gameObject.name, data);

                markerData = data;
            }
            else
            {
                print("load data:" + data.Description);
                markerData = data;

                //写真の復元
                Texture2D texture = Utilities.ReadPictureFromDataPath(data.ImageName);
                if (texture != null)
                {
                    this.imagePhoto.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                }
            }

            uiText_desc.text = data.Description;
        }

        public void SetNoteColor(Color color)
        {
            this.frame.GetComponent<Renderer>().material.color = color;
        }

        //次のノートに指定された時の動き
        public void SetNoteAsNextTarget()
        {
            AudioClip pong = Resources.Load<AudioClip>("decision3");
            GetComponent<AudioSource>().PlayOneShot(pong);
            GetComponent<FadeEffect>().StartBlinkColor(frame, SpatialNoteManager.Instance.NormalColor, SpatialNoteManager.Instance.NextColor);
        }

        public void SetEditButtonActive(bool active)
        {
            button_Remove.SetActive(active);
            button_Text.SetActive(active);
            button_Picture.SetActive(active);
        }

        public void SetSeqNo(int no,bool nextButtonVisible)
        {
            this.SeqNo = no;
            //表示のみ 1 start index
            this.uiText_seq.text = "Page " + (SeqNo + 1).ToString();

            button_Next.SetActive(nextButtonVisible);
        }

        // Update is called once per frame
        void Update(){}

        #region button_events

        bool CanUseButton
        {
            get
            {
                if (SpatialNoteManager.Instance.appMode != SpatialNoteManager.AppMode.Edit
                    || SpatialNoteManager.Instance.BehaveMode == SpatialNoteManager.EnumBehaveMode.PhotoCapture
                    || SpatialNoteManager.Instance.BehaveMode == SpatialNoteManager.EnumBehaveMode.Recording)
                {
                    return false;
                }
                return true;
            }
        }

        public void Remove_onClick()
        {
            if (!CanUseButton) return;

            //画像の削除
            Utilities.DeletePictureFromDataPath(markerData.ImageName);
            //WorldAnchorおよび自身の削除
            SpatialNoteManager.Instance.DeleteNote(gameObject);
        }

        public void Photo_onClick()
        {
            if (!CanUseButton) return;

            print("Capture Mode");
            SpatialNoteManager.Instance.SetPhotoCaptureMode(markerData.ImageName,this.gameObject);
        }

        public void StartRecord_onClick()
        {
            if (!CanUseButton) return;

            //ボタンをdisableにする
            this.button_Text.GetComponent<FadeEffect>().SetValueRecursive(0.1f);

            SpatialNoteManager.Instance.SetRecordingMode(marker: this.gameObject);
        }

        public void EndRecord()
        {
            this.button_Text.GetComponent<FadeEffect>().SetValueRecursive(1.0f);
            this.button_Text_label.text = "Edit Text";
        }

        public void SaveDescription(string description)
        {
            print("SaveDescription:" + description);
            
            this.markerData.Description = description;
            PlayerPrefsUtils.SetObject<MarkerData>(this.gameObject.name, markerData);
        }

        public void Next_onClick()
        {
            SpatialNoteManager.Instance.NextNoteNavigation(currentNo: SeqNo);
        }

        public void Detail_onClick()
        {
            var d = Instantiate(DetailCanvasPrefab);
            d.SetActive(true);
            if (SpatialNoteManager.Instance.detailWindow != null)
            {
                //print("detailWindow is not null");
                Destroy(SpatialNoteManager.Instance.detailWindow);
            }
            SpatialNoteManager.Instance.detailWindow = d;

            Vector3 ToCameraDirection = (gameObject.transform.position - CameraCache.Main.transform.position).normalized;

            d.transform.position = gameObject.transform.position - ToCameraDirection * 0.1f;
            //d.transform.rotation = gameObject.transform.rotation;

            d.GetComponent<NoteDetailBehaviour>().ImagePhoto.sprite = this.imagePhoto.sprite;
            d.GetComponent<NoteDetailBehaviour>().Text_Description.text = this.uiText_desc.text;
        }
#endregion
        /*
        #region Dictation
        public void OnDictationHypothesis(DictationEventData eventData)
        {
            //throw new NotImplementedException();
            uiText_desc.text = eventData.DictationResult;
        }

        public void OnDictationResult(DictationEventData eventData)
        {
            //throw new NotImplementedException();
            uiText_desc.text = eventData.DictationResult;
        }

        public void OnDictationComplete(DictationEventData eventData)
        {
            var description = eventData.DictationResult;

            uiText_desc.text = description;

            button_Text_label.text = "Set Text(C)";

            //データ保存
            markerData.Description = description;
            PlayerPrefsUtils.SetObject<MarkerData>(this.gameObject.name, markerData);

            //isRecording = false;
            StartCoroutine(DictationInputManager.StopRecording());
        }

        public void OnDictationError(DictationEventData eventData)
        {
            print("Dictation Error");

            //isRecording = false;
            StartCoroutine(DictationInputManager.StopRecording());
        }
        #endregion
        */
    }
}